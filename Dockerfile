FROM node:17-alpine3.14

ENV NODE_ENV=production

WORKDIR /home/node/app

COPY ["package.json", "package-lock.json*", "./"]


RUN npm install --production

COPY . .

EXPOSE 9000
 
CMD [ "node", "index.js" ]