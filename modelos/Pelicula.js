const mongoose = require('mongoose');

const PeliculaSchema = mongoose.Schema({
        Title:{
            type:String,
            require:true
        },
        Year:{
            type:Number,
            require:true
        },
        Released:{
            type:String,
            require:true
        },
        Genre:{
            type:String,
            require:true
        },
        Director:{
            type:String,
            require:true
        },
        Actors: {
            type:String,
            require:true
        },
        Plot:{
            type:String,
            require:true
        },
        Ratings:[{
            Source:{
                type: String
            },
            Value:{
                type:String
            }
        }]
});
module.exports = mongoose.model('Pelicula', PeliculaSchema);