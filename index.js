const Koa = require('koa');
const KoaRouter = require('koa-router');
const json = require('koa-json');
const render = require('koa-ejs');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('koa-bodyparser');

require("dotenv").config();

const PeliculasM = require('./modelos/Pelicula');


//DB mongo
console.log(process.env.MONGODB_URI);
mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true})
.then(()=> console.log("Conectado con: "+process.env.MONGODB_URI))
.catch((error)=> console.error(error));
//

const app= new Koa();
const router = new KoaRouter();

app.use(json());
//parser
app.use(bodyParser());

render(app, {
    root: path.join(__dirname, 'vistas'),
    layout:'layout',
    viewExt: 'html',
    cache:false,
    debug:false
});

router.get('/', listarPeliculas);
router.get('/peliculas', listarPeliculas);
router.post('/reemplazar', reemplazar);

async function listarPeliculas(ctx){
     //const pls= new mongoose.Schema(peliculasModelo);
    const {limite=5,desde=0, titulo='', anio='' } = ctx.request.query;
    let total=null;
    let res=null;
    if(titulo==='' && anio===''){
        total=await PeliculasM.countDocuments();
        res=await PeliculasM.find()
        .sort({Title:1})
        .skip(Number(desde))
        .limit(Number(limite));
    }else{
        if(titulo===''){
            total=await PeliculasM.countDocuments({Year:Number(anio)});

            res=await PeliculasM.find({Year:Number(anio)})
            .sort({Title:1})
            .skip(Number(desde))
            .limit(Number(limite));
        }
        if(anio===''){
            total=await PeliculasM.countDocuments({Title:{$regex:".*"+titulo+"*",$options:"i"}});
            res=await PeliculasM.find({Title:{$regex:".*"+titulo+"*",$options:"i"}})
            .sort({Title:1})
            .skip(Number(desde))
            .limit(Number(limite));
        }
        if(anio!='' && titulo!=''){
            total=await PeliculasM.countDocuments({Title:{$regex:".*"+titulo+"*",$options:"i"}, Year:Number(anio)});

            res=await PeliculasM.find({Title:{$regex:".*"+titulo+"*",$options:"i"}, Year:Number(anio)})
            .sort({Title:1})
            .skip(Number(desde))
            .limit(Number(limite));
        }
    }
    
    await ctx.render('listaPeliculas',{
        peliculas:res,
        limite:Number(limite),
        desde:Number(desde),
        siguiente:((Number(desde)+Number(limite))>=total?total-Number(limite):Number(desde)+Number(limite)),
        anterior:((Number(desde)-Number(limite)<0)?0:Number(desde)-Number(limite)),
        total,
        titulo,
        anio
    });   
}

async function reemplazar(ctx){
    const { idr, titulo, plot, pbuscar='vacio', preemplazar='vacio', resultado='vacio'} = ctx.request.body;
    let nuevoPlot='';
    if(pbuscar!='vacio'){
        let reg=await PeliculasM.findById(idr); 
        nuevoPlot=reg.Plot.replace(pbuscar, preemplazar);
        PeliculasM.findByIdAndUpdate(idr,{Plot:nuevoPlot}).exec();
    }
    await ctx.render('reemplazar',{
        idr,
        titulo,
        plot,
        resultado:nuevoPlot
    });
}

app.use(router.routes()).use(router.allowedMethods());

const port = process.env.PORT || 9000;

app.listen(9000, ()=>{
    console.log('escuchando en:'+port);
});